package com.example.model.user

import com.example.model.role.RoleDto

data class UserDto(val id: Int, val username: String, val password: String, val email: String, val roles: List<RoleDto>?)