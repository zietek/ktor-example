package com.example.model.user

import com.example.model.role.Role
import com.example.model.user.role.UserRoles
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass


class User(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<User>(Users)

    var username by Users.username
    var password by Users.password
    var email by Users.email
    var roles by Role via UserRoles

    fun toDto(): UserDto {
        return UserDto(id.value, username, password, email, roles.toList().map { it.toDto() })
    }
}