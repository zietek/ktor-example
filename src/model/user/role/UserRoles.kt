package com.example.model.user.role

import com.example.model.role.Roles
import com.example.model.user.Users
import org.jetbrains.exposed.sql.Table

object UserRoles : Table() {
    val user = reference("user", Users).primaryKey(0)
    val role = reference("role", Roles).primaryKey(1)
}