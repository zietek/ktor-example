package com.example.model.user

import org.jetbrains.exposed.dao.IntIdTable

object Users : IntIdTable() {
    val username = varchar("username", 50)
    val password = varchar("password", 50)
    val email = varchar("email", 100)
}