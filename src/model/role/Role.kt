package com.example.model.role

import com.example.model.user.UserDto
import com.example.model.user.Users
import com.example.model.user.role.UserRoles
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.select

class Role(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Role>(Roles)

    var name by Roles.name

    fun toDto(): RoleDto {
        return RoleDto(id.value, name, emptyList())
    }
}