package com.example.model.role

import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.Column

object Roles : IntIdTable() {
    val name: Column<String> = varchar("name", 50)
}