package com.example.model.role

import com.example.model.user.UserDto

data class RoleDto(val id: Int?, val name: String, val users: List<UserDto>?)