package com.example

import com.example.model.role.RoleDto
import com.example.model.role.Roles
import com.example.model.user.UserDto
import com.example.model.user.Users
import com.example.security.SimpleJWT
import com.example.service.*
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route

val simpleJWT = SimpleJWT("my-secret")


fun Routing.users() {
    authenticate {
        route("/users") {
            get {
                val users = Users.findUsers()
                call.respondText { users.joinToString(",") }
            }
        }
    }

    post("/login") {
        val post = call.receive<Login>()
        val user = Users.findUserByUsername(post.username) ?: throw ApplicationException(
            HttpStatusCode.Unauthorized,
            "Invalid credentials"
        )
        if (user.password != post.password) throw ApplicationException(
            HttpStatusCode.Unauthorized,
            "Invalid credentials"
        )
        call.respond(mapOf("token" to simpleJWT.sign(user.username)))
    }

    post("/register") {
        val post = call.receive<UserDto>()
        Users.registerUser(post)
        call.respond("OK")
    }
}

fun Routing.roles() {
    authenticate {
        route("/roles") {
            get {
                call.respondText { Roles.findAllRoles().joinToString(",") }
            }
            post {
                val role = call.receive<RoleDto>()
                Roles.createRole(role.name)
                call.respond("OK")
            }
        }
    }
}
