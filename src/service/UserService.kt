package com.example.service

import com.example.ApplicationException
import com.example.model.role.Role
import com.example.model.user.User
import com.example.model.user.UserDto
import com.example.model.user.Users
import io.ktor.http.HttpStatusCode
import org.jetbrains.exposed.sql.transactions.transaction

fun Users.findUsers(): List<UserDto> {
    return transaction {
        User.all().toList().map { it.toDto() }
    }
}

fun Users.registerUser(post: UserDto) {
    transaction {
        if (!User.find { Users.username like post.username }.empty()) {
            throw ApplicationException(HttpStatusCode.Conflict, "username with same name already exists")
        }
        User.new {
            username = post.username
            password = post.password
            email = post.email
        }
    }

}

fun Users.initializeDefaultUser() {
    transaction {
        if (User.find { Users.username like "admin" }.empty()) {
            val user = User.new {
                username = "admin"
                email = "admin@email.com"
                password = "wkswks12"
            }
            user.roles = Role.all() // you can't do it during creation
        }
    }
}

fun Users.findUserByUsername(username: String): User? {
    return transaction {
        User.find { Users.username like username }.firstOrNull()
    }
}
