package com.example.service

import com.example.ApplicationException
import com.example.model.role.Role
import com.example.model.role.RoleDto
import com.example.model.role.Roles
import com.example.model.user.UserDto
import com.example.model.user.Users
import com.example.model.user.role.UserRoles
import io.ktor.http.HttpStatusCode
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

fun Roles.createRole(roleName: String) {
    transaction {
        if (!Roles.select { Roles.name eq roleName }.empty()) {
            throw ApplicationException(HttpStatusCode.Conflict, "Role with same name already exists")
        }
        val id = Roles.insertAndGetId {
            it[name] = roleName
        }
    }
}

fun Roles.findAllRoles(): List<RoleDto> {
    return transaction {
        Roles.selectAll().map {
            RoleDto(id = it[Roles.id].value, name = it[Roles.name],
                users = Users.join(UserRoles, JoinType.LEFT, additionalConstraint = { Users.id eq UserRoles.user })
                    .select { UserRoles.role eq it[Roles.id] }.distinct().map {
                        UserDto(
                            id = it[Users.id].value,
                            username = it[Users.username],
                            password = it[Users.password],
                            email = it[Users.email],
                            roles = emptyList()
                        )
                    })
        }.toList()
    }

}

fun Roles.initializeDefaultRole() {
    transaction {
        if (Role.find { Roles.name like "admin" }.empty()) {
            Role.new { name = "admin" }
        }
    }
}
