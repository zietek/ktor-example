package com.example

import com.example.model.role.Roles
import com.example.model.user.Users
import com.example.model.user.role.UserRoles
import com.example.security.SimpleJWT
import com.example.service.initializeDefaultRole
import com.example.service.initializeDefaultUser
import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.jwt.jwt
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.routing
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT) // Pretty Prints the JSON
        }
    }
    val simpleJWT = SimpleJWT("my-secret")
    install(Authentication) {
        jwt {
            verifier(simpleJWT.verifier)
            validate {
                UserIdPrincipal(it.payload.getClaim("name").asString())
            }
        }
    }
    install(StatusPages) {
        exception<ApplicationException> { exception ->
            call.respond(exception.statusCode, mapOf("Ok" to false, "error" to (exception.message ?: "")))
        }
    }

    //initialize default user
    Database.connect(
        url = "jdbc:mysql://localhost:3306/ktor", user = "root", password = "wkswks12",
        driver = "com.mysql.cj.jdbc.Driver"
    )
    transaction { SchemaUtils.create(Roles, Users, UserRoles) }
    Roles.initializeDefaultRole()
    Users.initializeDefaultUser()

    routing {
        users()
        roles()
    }
}

class Login(val username: String, val password: String)
class ApplicationException(val statusCode: HttpStatusCode, message: String) : RuntimeException(message)
